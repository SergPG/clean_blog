<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clean_blog
 */

get_header();
?>




		<?php

          



		while ( have_posts() ) :
			the_post();

		?>	
			<!-- Page Header -->
		    <header class="masthead" style="background-image: url('<?php the_post_thumbnail_url( array(1900, 1267) ); ?>')">
		      <div class="overlay"></div>
		      <div class="container">
		        <div class="row">
		          <div class="col-lg-8 col-md-10 mx-auto">
		            <div class="site-heading">

		              <?php the_title( '<h1>', '</h1>' ); ?>
		    
		              <span class="subheading">A Blog Theme by Start Bootstrap</span>
		            </div>
		          </div>
		        </div>
		      </div>
		    </header>

		<?php	

             if( is_front_page() ){




				 get_template_part( 'template-parts/content', 'home' );
			}
			else {
				//get_template_part( 'template-parts/content', 'page' );
			}





		endwhile; // End of the loop.
		?>

<?php
get_footer();
?>
