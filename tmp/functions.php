
<?php


/**
 * Sets up theme defaults WordPress.
 */
function lessontheme_setup() {

// This theme uses wp_nav_menu() in two locations.
	register_nav_menus(
		array(
			'mainNav'    => 'Верхнее меню',    //Название месторасположения меню в шаблоне
			'bottom' => 'Нижнее меню'   
		)
	);

}
add_action( 'after_setup_theme', 'lessontheme_setup' );





// Изменяет основные параметры меню
add_filter( 'wp_nav_menu_args', 'filter_wp_menu_args' );
function filter_wp_menu_args( $args ) {
	if ( $args['theme_location'] === 'mainNav' ) {
		$args['container']  = 'div';
		$args['container_class']  = 'collapse navbar-collapse';
		$args['container_id']  = 'main-nav';
		//$args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
		$args['menu_class'] = 'nav navbar-nav';
		$args['menu_id'] = 'mainNav';
	}

	return $args;
}

// Изменяем атрибут id у тега li
add_filter( 'nav_menu_item_id', 'filter_menu_item_css_id', 10, 4 );
function filter_menu_item_css_id( $menu_id, $item, $args, $depth ) {

	if ( $args->theme_location === 'mainNav' ) {
		
		if ( $item->menu_order === 1 ) {
			$menu_id = 'firstLink';	
		}
		else {
			$menu_id = '';
		}
	}
	return $menu_id;	
}

// Изменяем атрибут class у тега li
add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( $args->theme_location === 'mainNav' ) {
		 $classes = ['menu_' . ( $depth + 1 )];
       
        if ( $item->menu_order === 1 ) {
			$classes[] = 'active';
			
		}
	}

	return $classes;
}

// Изменяет класс у вложенного ul
// add_filter( 'nav_menu_submenu_css_class', 'filter_nav_menu_submenu_css_class', 10, 3 );
// function filter_nav_menu_submenu_css_class( $classes, $args, $depth ) {
// 	if ( $args->theme_location === 'mainNav' ) {
// 		$classes = [
// 			'menu',
// 			'menu--dropdown',
// 			'menu--vertical'
// 		];
// 	}

// 	return $classes;
// }


// ДОбавляем классы ссылкам
add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
	if ( $args->theme_location === 'mainNav' ) {
		$atts['class'] = 'scroll-link';
	}

	return $atts;
}



// Remove themes old version of jQuery and load a compatible version
function my_update_jquery () {
	if ( !is_admin() ) { 
	   wp_deregister_script('jquery');
 	   wp_register_script('jquery', get_theme_file_uri('/assets/js/jquery-1.8.2.min.js'), false, false, true);
	   wp_enqueue_script('jquery');
	}
}
add_action('wp_enqueue_scripts', 'my_update_jquery');


/**
 * Enqueues scripts and styles.

 D:\laragon\www\myBlog.loc\wp-content\themes\lessontheme\assets\css
 */

function lessontheme_scripts() {
	
	// Theme stylesheet.

	wp_enqueue_style( 'lessontheme-bootstrap-style', get_theme_file_uri( '/assets/css/bootstrap.min.css' ) );
	wp_enqueue_style( 'lessontheme-isotope-style', get_theme_file_uri( '/assets/css/isotope.css' ) );
	wp_enqueue_style( 'lessontheme-fancybox-style', get_theme_file_uri( '/assets/js/fancybox/jquery.fancybox.css' ) );
	wp_enqueue_style( 'lessontheme-animate-style', get_theme_file_uri( '/assets/css/animate.css' ) );
	wp_enqueue_style( 'lessontheme-flexslider-style', get_theme_file_uri( '/assets/flexslider/flexslider.css' ) );
	wp_enqueue_style( 'lessontheme-carousel-style', get_theme_file_uri( '/assets/js/owl-carousel/owl.carousel.css' ) );

	wp_enqueue_style( 'lessontheme-style', get_stylesheet_uri() );

	// Font Awesome 
	wp_enqueue_style( 'lessontheme-font-awesome-style', get_theme_file_uri( '/assets/font/css/font-awesome.min.css' ) );

	// Theme script.

	wp_enqueue_script( 'lessontheme-modernizr-latest', get_theme_file_uri( '/assets/js/modernizr-latest.js' ), array(), null, true );

   // wp_enqueue_script('jquery');

    wp_enqueue_script( 'lessontheme-bootstrap-js', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-isotope-js', get_theme_file_uri( '/assets/js/jquery.isotope.min.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-fancybox-js', get_theme_file_uri( '/assets/js/fancybox/jquery.fancybox.pack.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-nav-js', get_theme_file_uri( '/assets/js/jquery.nav.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-mapsurl-js','http://maps.google.com/maps/api/js?sensor=true',array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-gmaps-js', get_theme_file_uri( '/assets/js/gmaps.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-fittext-js', get_theme_file_uri( '/assets/js/jquery.fittext.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-waypoints-js', get_theme_file_uri( '/assets/js/waypoints.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-flexslider-js', get_theme_file_uri( '/assets/flexslider/jquery.flexslider.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-jqBootstrapValidation-js', get_theme_file_uri( '/assets/contact/jqBootstrapValidation.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-contact_me-js', get_theme_file_uri( '/assets/contact/contact_me.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-custom_les-js', get_theme_file_uri( '/assets/js/custom.js' ), array('jquery'), null, true );
    wp_enqueue_script( 'lessontheme-owl-carousel-js', get_theme_file_uri( '/assets/js/owl-carousel/owl.carousel.js' ), array('jquery'), null, true );
}
add_action( 'wp_enqueue_scripts', 'lessontheme_scripts' );




?>