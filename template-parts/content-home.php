<?php
/**
 * Template part for displaying page content in home.php
 *
 * @package clean_blog
 */

?>


    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">

       
         


   <?php
// 1 значение по умолчанию
$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$the_query = new WP_Query( array(
  'posts_per_page' => 2,
  'post_type'   => 'post',
  'paged'          => $paged,
) );

// цикл вывода полученных записей
while( $the_query->have_posts() ){
  $the_query->the_post();
  ?>
  <!-- HTML каждой записи -->
    <div class="post-preview">
            <a href="<?php the_permalink(); ?>">

               <?php the_title( ' <h2 class="post-title">', '</h2>' ); ?>

              <h3 class="post-subtitle">
                Problems look mighty small from 150 miles up
              </h3>
            </a>
            <p class="post-meta">Posted by
              <a href="#"><?php the_author(); ?></a>
              <?php the_time('j F Y'); ?></p>
          </div>
          <hr>




  <?php 
} 
wp_reset_postdata();

// пагинация для произвольного запроса
$big = 999999999; // уникальное число

echo paginate_links( array(
  'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
  'format'  => '?paged=%#%',
  'current' => max( 1, get_query_var('paged') ),
  'total'   => $the_query->max_num_pages
) );
?>

   
         

     

         
          


          <!-- Pager -->
          <div class="clearfix">
            <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
          </div>



        </div>
      </div>
    </div>

